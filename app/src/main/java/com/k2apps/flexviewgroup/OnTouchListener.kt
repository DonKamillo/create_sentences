package com.k2apps.flexviewgroup

import android.view.View

interface OnTouchListener {

    fun startDrag(view: View, index: Int, isFromTopContainer: Boolean)
}