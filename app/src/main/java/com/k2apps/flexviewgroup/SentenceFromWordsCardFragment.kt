package com.google.android.flexbox

import android.animation.AnimatorSet
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.k2apps.flexviewgroup.FLexCardListener
import com.k2apps.flexviewgroup.R
import com.k2apps.flexviewgroup.utils.ANIMATION_DURATION
import com.k2apps.flexviewgroup.utils.FlexCard
import com.k2apps.flexviewgroup.utils.getViewItemToScalingAnimator
import kotlinx.android.synthetic.main.fragment_sentence_card_from_words.*


class SentenceFromWordsCardFragment : Fragment(), FLexCardListener {

    private lateinit var flexCard: FlexCard

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sentence_card_from_words, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        next_btn.setOnClickListener { onNextButtonClick() }
        help.setOnClickListener { flexCard.onHelpClick() }

        flexCard = FlexCard(
            this,
            view.findViewById(R.id.top_flex_layout),
            view.findViewById(R.id.bottom_flex_layout)
        )
        title.text = PL

        setUpHelpButton()
    }

    override fun getCorrectSentence(): List<String> {
        return CORRECT_WORDS
    }

    override fun getAdditionalWords(): List<String> {
        return ADDITIONAL_WORDS
    }

    override fun createShuttleAnimationSet(
        fromRect: Rect,
        toRect: Rect,
        shuttleView: View
    ): AnimatorSet {
        return getViewItemToScalingAnimator(
            root,
            shuttleView,
            fromRect,
            toRect,
            ANIMATION_DURATION.toLong(),
            0
        )
    }

    override fun createShuttleView(text: String): View {
        val shuttleView = activity!!.layoutInflater.inflate(R.layout.item_sentence_shuttle, null)
        val itemText = shuttleView?.findViewById<TextView>(R.id.itemText)
        shuttleView.visibility = View.INVISIBLE
        itemText?.text = text
        root.addView(shuttleView)
        return shuttleView!!
    }

    override fun createWordView(): View {
        return activity!!.layoutInflater.inflate(R.layout.item_sentence_word, null)
    }

    override fun createShadowView(): View {
        return activity!!.layoutInflater.inflate(R.layout.item_sentence_word, null)
    }

    override fun setHelpEnable(enable: Boolean) {
        help.isEnabled = enable
    }

    override fun setHelpText(tips: Int) {
        help.text = getString(R.string.add_word, tips)
    }

    override fun colorWord(wordView: View?, isCorrect: Boolean) {
        val itemText = wordView?.findViewById<TextView>(R.id.itemText)
        context?.resources?.getColor(if (isCorrect) R.color.colorPrimary else R.color.red)
            ?.let { itemText?.setTextColor(it) }
    }

    private fun setUpHelpButton() {
        help.isEnabled = true

        flexCard.helpLetters = when {
            flexCard.correctSentence.size < 5 -> 2
            flexCard.correctSentence.size < 7 -> 3
            flexCard.correctSentence.size < 9 -> 4
            else -> 5
        }
        help.text =
            getString(
                R.string.add_word,
                flexCard.helpLetters
            )
    }


    private fun onNextButtonClick() {
        flexCard.colorNeutralWords()
        if (flexCard.isCorrectAnswer()) {
            // idz do nastepnej karty
            correctAnswer()
        } else {
            // usuń gwiazdkę
            wrongAnswer()
            flexCard.colorWrongWords()
        }
    }

    private fun correctAnswer() {

    }

    private fun wrongAnswer() {

    }

    companion object {
        private const val TAG = "WritingCardFragment"
        private val PL = "I do not like this company"
        private val CORRECT_WORDS = listOf("Nie", "lubię", "posiadać", "samochodu")
        private val ADDITIONAL_WORDS =
            listOf("mniejszy", "dotykało", "dachy", "dopełniacz")

        fun newInstance(): SentenceFromWordsCardFragment {
            return SentenceFromWordsCardFragment()
        }
    }
}