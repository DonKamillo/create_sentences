package com.k2apps.flexviewgroup

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.flexbox.WritingCardFragment
import com.google.android.flexbox.SentenceFromWordsCardFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var isWord = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceToFlexboxLayoutFragment(supportFragmentManager, isWord)

        type.setOnClickListener {
            isWord = !isWord
            replaceToFlexboxLayoutFragment(supportFragmentManager, isWord)
        }

    }


    private fun replaceToFlexboxLayoutFragment(fragmentManager: FragmentManager, isWord: Boolean) {
        val fragment: Fragment =
            if (isWord) WritingCardFragment.newInstance() else SentenceFromWordsCardFragment.newInstance()
        fragmentManager.beginTransaction()
            .replace(R.id.container, fragment, FLEXBOXLAYOUT_FRAGMENT).commit()
    }

    companion object {
        private const val FLEXBOXLAYOUT_FRAGMENT = "flexboxlayout_fragment"

    }
}
