package com.k2apps.flexviewgroup

import android.view.View

interface OnDragListener {
    fun dragStartedInTopContainer(bottomView: View, draggedViewId: Int)
    fun dragStartedInBottomContainer(bottomView: View, draggedViewId: Int)
    fun dragEnteredWord(bottomView: View, draggedViewId: Int)
    fun dragEnteredTopContainer(bottomView: View, draggedViewId: Int)
    fun dragLocation(bottomView: View, draggedViewId: Int, x: Float)
    fun dragExitedWord(bottomView: View, draggedViewId: Int)
    fun dragExitedTopContainer(bottomView: View, draggedViewId: Int)
    fun dropInTopContainer(
        bottomView: View,
        draggedViewId: Int,
        isDragViewFromTopContainer: Boolean
    )

    fun dropInBottomContainer(
        bottomView: View,
        draggedViewId: Int,
        isDragViewFromTopContainer: Boolean
    )

    fun dragEnded()
}