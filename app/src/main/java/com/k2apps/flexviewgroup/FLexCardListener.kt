package com.k2apps.flexviewgroup

import android.animation.AnimatorSet
import android.graphics.Rect
import android.view.View

interface FLexCardListener {
    fun colorWord(wordView: View?, isCorrect: Boolean)
    fun setHelpEnable(enable: Boolean)
    fun setHelpText(tips: Int)
    fun createShadowView(): View
    fun createWordView(): View
    fun createShuttleAnimationSet(
        fromRect: Rect,
        toRect: Rect,
        shuttleView:View
    ): AnimatorSet
    fun getCorrectSentence(): List<String>
    fun getAdditionalWords(): List<String>
    fun createShuttleView(text: String): View
}