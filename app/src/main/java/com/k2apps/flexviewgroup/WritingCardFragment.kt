package com.google.android.flexbox

import android.animation.AnimatorSet
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.k2apps.flexviewgroup.FLexCardListener
import com.k2apps.flexviewgroup.R
import com.k2apps.flexviewgroup.utils.*
import kotlinx.android.synthetic.main.fragment_card_writing.*
import kotlinx.android.synthetic.main.fragment_sentence_card_from_words.help
import kotlinx.android.synthetic.main.fragment_sentence_card_from_words.next_btn
import kotlinx.android.synthetic.main.fragment_sentence_card_from_words.title


class WritingCardFragment : Fragment(), FLexCardListener {

    private lateinit var flexCard: FlexCard
    val word: Word = Word(PL_WORD, 2)

    protected var progressIcon: ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_card_writing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        next_btn.setOnClickListener { onNextButtonClick() }
        help.setOnClickListener { flexCard.onHelpClick() }

        flexCard = FlexCard(
            this,
            view.findViewById(R.id.top_flex_layout),
            view.findViewById(R.id.bottom_flex_layout)
        )

        title.text = PL_WORD
        progressIcon?.setImageResource(Utils.getWordProgressIconRes(word.progress))

        setUpHelpButton()
        setUpMainImage()
    }

    override fun getCorrectSentence(): List<String> {
        return CORRECT_LETTERS
    }

    override fun getAdditionalWords(): List<String> {
        return ADDITIONAL_LETTERS
    }

    override fun createShuttleAnimationSet(
        fromRect: Rect,
        toRect: Rect,
        shuttleView: View
    ): AnimatorSet {
        return getViewItemToScalingAnimator(
            root,
            shuttleView,
            fromRect,
            toRect,
            ANIMATION_DURATION.toLong(),
            0
        )
    }

    override fun createShuttleView(text: String): View {
        val shuttleView = activity!!.layoutInflater.inflate(R.layout.item_word_shuttle, null)
        val itemText = shuttleView?.findViewById<TextView>(R.id.itemText)
        shuttleView.visibility = View.INVISIBLE
        itemText?.text = text
        root.addView(shuttleView)
        return shuttleView!!
    }

    override fun createWordView(): View {
        return activity!!.layoutInflater.inflate(R.layout.item_word_letter, null)
    }

    override fun createShadowView(): View {
        return activity!!.layoutInflater.inflate(R.layout.item_word_letter, null)
    }

    override fun setHelpEnable(enable: Boolean) {
        help.isEnabled = enable
    }

    override fun setHelpText(tips: Int) {
        help.text = getString(R.string.add_letter, tips)
    }

    override fun colorWord(wordView: View?, isCorrect: Boolean) {
        val itemText = wordView?.findViewById<TextView>(R.id.itemText)
        context?.resources?.getColor(if (isCorrect) R.color.colorPrimary else R.color.red)
            ?.let { itemText?.setTextColor(it) }
    }

    private fun setUpMainImage() {
        val img = Utils.getWordImageRes(context!!, word.infinitive)
        if (img == 0) {
            word_image.visibility = View.INVISIBLE
            word_image.layoutParams.height = (25 * resources.displayMetrics.density).toInt()
        } else {
            word_image.visibility = View.VISIBLE
            word_image.setImageResource(img)
        }
    }

    private fun setUpHelpButton() {
        help.isEnabled = true

        flexCard.helpLetters = when {
            flexCard.correctSentence.size < 5 -> 2
            flexCard.correctSentence.size < 7 -> 3
            flexCard.correctSentence.size < 9 -> 4
            else -> 5
        }
        help.text =
            getString(
                R.string.add_letter,
                flexCard.helpLetters
            )
    }


    private fun onNextButtonClick() {
        flexCard.colorNeutralWords()
        if (flexCard.isCorrectAnswer()) {
            // idz do nastepnej karty
            correctAnswer()
        } else {
            // usuń gwiazdkę
            wrongAnswer()
            flexCard.colorWrongWords()
        }
    }

    private fun correctAnswer() {

    }

    private fun wrongAnswer() {

    }

    companion object {
        private const val TAG = "SentenceFromWordsCardFragment"
        val PL_WORD = "COMPANY"
        private val CORRECT_LETTERS = listOf("C", "O", "M", "P", "A", "N", "Y")
        private val ADDITIONAL_LETTERS =
            listOf("A", "B", "G", "P", "W")

        fun newInstance(): WritingCardFragment {
            return WritingCardFragment()
        }
    }
}