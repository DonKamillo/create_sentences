package com.k2apps.flexviewgroup.utils

enum class DragObject {
    TOP_CONTAINER,
    BOTTOM_CONTAINER,
    TOP_WORD,
    BOTTOM_WORD,
}