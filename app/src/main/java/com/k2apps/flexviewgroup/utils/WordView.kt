package com.k2apps.flexviewgroup.utils

import android.view.View

data class WordView(val id: Int, val word: String, var view: View, var shuttle: View, var isVisible: Boolean = true)