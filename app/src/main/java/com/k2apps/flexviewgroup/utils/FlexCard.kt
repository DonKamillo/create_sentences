package com.k2apps.flexviewgroup.utils

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.LayoutTransition
import android.content.ClipData
import android.graphics.Rect
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import android.widget.TextView
import androidx.core.view.contains
import androidx.core.view.get
import androidx.core.view.iterator
import androidx.core.view.size
import com.google.android.flexbox.FlexboxLayout
import com.k2apps.flexviewgroup.FLexCardListener
import com.k2apps.flexviewgroup.OnDragListener
import com.k2apps.flexviewgroup.OnTouchListener
import com.k2apps.flexviewgroup.R


class FlexCard(
    val listener: FLexCardListener,
    val flexContainerTop: FlexboxLayout,
    val flexContainerBottom: FlexboxLayout
) : OnDragListener,
    OnTouchListener {

    private var wordId = 0
    var helpLetters = 0
    private var shadowView: View? = null
    lateinit var correctSentence: List<String>
    private val wordsTop = mutableListOf<WordView>()
    private val wordsBottom = mutableListOf<WordView>()
    private var shuttleAnimatorSet: AnimatorSet? = null
    private var dragEnded = false
    private var viewAddedToTopContainerByClick = false
    private var isJustDroppedWordInTopContainer = false
    private var isJustDroppedWordInBottomContainer = false
    private var isDraggedViewFromTopContainer: Boolean = false
    private var viwIndexAfterClickedBottomViewList: Int =
        BOTTOM_VIEW_CLICK_INDEX_DEFAULT
    private var draggedId = DRAGGED_ID_DEFAULT
    private var draggedViewIndexInTopLayout = 0

    private var dragExitedTimer = object : CountDownTimer(1000, 100) {
        override fun onTick(millisUntilFinished: Long) {
        }

        override fun onFinish() {
            addShadowViewToStartPosition()
        }
    }


    init {
        setUpFlexContainers()
        createShadowView()
        initSentence()
    }

    fun onHelpClick() {
        if (helpLetters == 0) {
            return
        }

        if (getAnswer().isEmpty()) {
            addCorrectWord()
        } else {
            val wordToAdd = correctSentence[getLastCorrectWord() + 1]
            addNextCorrectWord(wordToAdd)
        }

        if (helpLetters > 0) {
            helpLetters--
            listener.setHelpText(helpLetters)

        }
        if (helpLetters == 0) {
            listener.setHelpEnable(false)
        }
    }

    private fun addShadowViewToStartPosition() {
        shadowView?.let {
            if (flexContainerTop.contains(it) && flexContainerTop.indexOfChild(it) != draggedViewIndexInTopLayout) {
                flexContainerTop.removeView(it)
                flexContainerTop.addView(it, draggedViewIndexInTopLayout)
            }
        }
    }


    private fun setUpFlexContainers() {
        flexContainerTop.removeAllViews()
        flexContainerBottom.removeAllViews()

        // turn off animation when disappearing after click view
        val lt = LayoutTransition()
        lt.disableTransitionType(LayoutTransition.DISAPPEARING)
        flexContainerTop.layoutTransition = lt
        flexContainerTop.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ -> topLayoutChange() }
        flexContainerTop.setOnDragListener(DragListener(this, DragObject.TOP_CONTAINER, "-"))
        flexContainerBottom.setOnDragListener(DragListener(this, DragObject.BOTTOM_CONTAINER, "-"))
    }


    private fun topLayoutChange() {
        // animation run after click word in bottom container, not after drag&drop
        if (viwIndexAfterClickedBottomViewList != BOTTOM_VIEW_CLICK_INDEX_DEFAULT && viewAddedToTopContainerByClick) {
            val bottomWordView = getBottomWord(viwIndexAfterClickedBottomViewList)
            val topWordView = getTopWord(viwIndexAfterClickedBottomViewList)

            bottomWordView?.let {
                topWordView?.let {
                    animAfterClick(viwIndexAfterClickedBottomViewList, false)
                    viwIndexAfterClickedBottomViewList =
                        BOTTOM_VIEW_CLICK_INDEX_DEFAULT
                }
            }
        }
    }

    private fun createShadowView() {
        shadowView = listener.createShadowView()
        shadowView?.tag = SHADOW_TAG
        shadowView?.setOnDragListener(
            DragListener(this, DragObject.TOP_WORD, "shadow")
        )
        shadowView?.layoutParams = setFlexItemAttributes()
    }

    private fun initSentence() {
        correctSentence = listener.getCorrectSentence()
        val additionalWords = listener.getAdditionalWords()

        for (w in correctSentence.plus(additionalWords).shuffled()) {
            val id = wordId++
            val topView = createWordView(id, w, true, listener.createWordView())
            val bottomView = createWordView(id, w, false, listener.createWordView())

            val shuttle = listener.createShuttleView(w)

            wordsTop.add(WordView(id, w, topView, shuttle))
            wordsBottom.add(WordView(id, w, bottomView, shuttle))


            flexContainerBottom.addView(bottomView)
        }
    }


    private fun addCorrectWord() {
        if (flexContainerTop.childCount >= correctSentence.size) {
            return
        }
        val wordToAdd = correctSentence[flexContainerTop.childCount]

        for (wordView in wordsBottom) {
            if (wordView.word == wordToAdd && wordView.isVisible) {
                for (v in flexContainerBottom) {
                    if (v.tag == wordView.id) {
                        v.callOnClick()
                        return
                    }
                }
            }
        }
    }

    private fun getLastCorrectWord(): Int {
        var lastCorrectWordIndex = 0
        getAnswer().forEachIndexed { index, s ->
            lastCorrectWordIndex = index
            if (index >= correctSentence.size) {
                return -1
            } else {
                val correctWord = correctSentence[index]
                if (s != correctWord) {
                    return index - 1
                }
            }
        }
        return lastCorrectWordIndex
    }

    private fun addNextCorrectWord(wordToAdd: String) {
        var indexToRemove = listOf<Int>()
        var isWrong = false
        getAnswer().forEachIndexed { index, s ->
            if (index >= correctSentence.size || isWrong) {
                indexToRemove = indexToRemove.plus(index)
            } else {
                val correctWord = correctSentence[index]
                if (s != correctWord) {
                    indexToRemove = indexToRemove.plus(index)
                    isWrong = true
                }
            }
        }

        var wordToAddInIndexesToRemove = -1
        for (i in indexToRemove) {
            val isViewWithNextCorrectWord =
                isViewInTopContainerWithGivenTagIsViewWithNextCorrectWord(
                    flexContainerTop[i].tag as Int,
                    wordToAdd
                )

            if (isViewWithNextCorrectWord) {
                wordToAddInIndexesToRemove = i
                break
            }
        }
        for (i in indexToRemove.sortedDescending()) {
            if (wordToAddInIndexesToRemove != i)
                flexContainerTop[i].callOnClick()
        }

        if (wordToAddInIndexesToRemove == -1) {
            addCorrectWord()
        }
    }

    private fun isViewInTopContainerWithGivenTagIsViewWithNextCorrectWord(
        viewTag: Int,
        nextCorrectWord: String
    ): Boolean {
        for (wordView in wordsTop) {
            if (wordView.view.tag == viewTag && wordView.word == nextCorrectWord) {
                return true
            }
        }
        return false
    }

    fun colorNeutralWords() {
        for (w in wordsTop) {
            listener.colorWord(w.view, true)
        }
    }

    fun colorWrongWords() {
        var isWrong = false
        getAnswer().forEachIndexed { index, s ->
            if (index >= correctSentence.size || isWrong) {
                listener.colorWord(flexContainerTop[index], false)
            } else {
                val correctWord = correctSentence[index]
                if (s != correctWord) {
                    listener.colorWord(flexContainerTop[index], false)
                    isWrong = true
                }
            }
        }
    }

    private fun setAllWordsColor() {
        for (v in flexContainerTop) {
            listener.colorWord(v, true)
        }
    }

    fun isCorrectAnswer(): Boolean {
        return correctSentence.joinToString(separator = "") == getAnswer().joinToString(separator = "")
    }

    private fun getAnswer(): List<String> {
        var answer = listOf<String>()
        for (v in flexContainerTop) {
            val w = getTopWord(v.tag as Int)?.word
            w?.let { answer = answer.plus(it) }
        }

        return answer
    }

    private fun animAfterClick(index: Int, fromTopLayout: Boolean) {
        val itemFrom: View
        val itemTo: View
        val shuttle = getTopWord(index)?.shuttle!!
        val topView = getTopWord(index)?.view
        val bottomView = getBottomWord(index)?.view

        if (topView == null || bottomView == null) {
            return
        }

        if (fromTopLayout) {
            itemFrom = topView
            itemTo = bottomView
        } else {
            itemFrom = bottomView
            itemTo = topView
        }

        val fromRect = Rect()
        val toRect = Rect()
        itemFrom.getGlobalVisibleRect(fromRect)
        itemTo.getGlobalVisibleRect(toRect)

        val shuttleAnimatorSet = listener.createShuttleAnimationSet(fromRect, toRect, shuttle)
        shuttleAnimatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                shuttle.visibility = View.VISIBLE

                if (!fromTopLayout) {
                    itemTo.visibility = View.INVISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animator) {
                finishAnimation(index, fromTopLayout, itemTo, shuttle)
            }

            override fun onAnimationCancel(animation: Animator) {
                finishAnimation(index, fromTopLayout, itemTo, shuttle)
            }

            override fun onAnimationRepeat(animation: Animator) {}
        })

        shuttleAnimatorSet.start()

    }

    private fun finishAnimation(
        index: Int,
        fromTopLayout: Boolean,
        itemTo: View,
        shuttleView: View
    ) {
        if (fromTopLayout) {
            setBottomWordViewVisible(getBottomWord(index), true)
        } else {
            itemTo.visibility = View.VISIBLE
        }


        val handler = Handler()
        handler.postDelayed({
            shuttleView.visibility = View.GONE
        }, REMOVE_SHUTTLE_AFTER_ANIMATION_DELAY)


    }

    private fun updateShadowViewText(text: String) {
        shadowView?.tag = SHADOW_TAG
        val itemText = shadowView?.findViewById<TextView>(R.id.itemText)
        itemText?.text = text
    }


    private fun createWordView(index: Int, text: String, isTop: Boolean, view: View): View {
        val itemText = view.findViewById<TextView>(R.id.itemText)
        itemText.text = text
        view.tag = index
        view.layoutParams = setFlexItemAttributes()

        if (isTop) {
            addTopListeners(view, index)
        } else {
            addBottomListeners(view, index)
        }

        return view
    }

    private fun addTopListeners(view: View, index: Int) {
        view.setOnDragListener(DragListener(this, DragObject.TOP_WORD, "$index"))
        TouchListener(view, index, true, this)

        view.setOnClickListener {
            setAllWordsColor()
            animAfterClick(index, true)
            flexContainerTop.removeView(view)
        }
    }

    private fun addBottomListeners(view: View, index: Int) {
        TouchListener(view, index, false, this)

        view.setOnClickListener {
            getBottomWord(index)?.isVisible?.let {
                if (it) {
                    setAllWordsColor()
                    viewAddedToTopContainerByClick = true
                    viwIndexAfterClickedBottomViewList = index
                    addViewToTopContainer(index)
                }
            }
        }
    }

    private fun addViewToTopContainer(index: Int) {
        val bottomWordView = getBottomWord(index)
        val topWordView = getTopWord(index)

        bottomWordView?.let {
            topWordView?.let {
                flexContainerTop.removeView(topWordView.view)
                flexContainerTop.addView(topWordView.view)
                setBottomWordViewVisible(bottomWordView, false)
            }
        }
    }

    // OnTouchListener
    override fun startDrag(view: View, index: Int, isFromTopContainer: Boolean) {
        isDraggedViewFromTopContainer = isFromTopContainer
        setAllWordsColor()

        val bottomWordView = getBottomWord(index)
        if (!isFromTopContainer && (bottomWordView == null || !bottomWordView.isVisible))
            return

        draggedViewIndexInTopLayout = if (isFromTopContainer) {
            flexContainerTop.indexOfChild(view)
        } else {
            flexContainerTop.size
        }

        createDragShadowBuilder(view, index, isFromTopContainer)

        isJustDroppedWordInBottomContainer = false
        isJustDroppedWordInTopContainer = false
        dragEnded = false
        draggedId = index
    }

    private fun createDragShadowBuilder(view: View, index: Int, isFromTopContainer: Boolean) {
        val data = ClipData.newPlainText(if (isFromTopContainer) "T$index" else "B$index", "")
        val shadowBuilder = View.DragShadowBuilder(view)
        view.startDrag(data, shadowBuilder, view, View.DRAG_FLAG_OPAQUE)
    }

    private fun changeWordViewToShadowView(view: View, text: String) {
        shadowView = view
        shadowView?.tag = SHADOW_TAG
        updateShadowViewText(text)

        setTopWordViewLookLikeShadow(shadowView, true)
    }

    // OnDragListener
    override fun dragStartedInTopContainer(bottomView: View, draggedViewId: Int) {
        val topWordView = getTopWord(draggedViewId)
        if (topWordView != null)
            changeWordViewToShadowView(topWordView.view, topWordView.word)

    }

    override fun dragStartedInBottomContainer(bottomView: View, draggedViewId: Int) {
        setBottomWordViewVisible(getBottomWord(draggedViewId), false)
    }

    override fun dragEnteredTopContainer(bottomView: View, draggedViewId: Int) {
        dragExitedTimer.cancel()
    }

    override fun dragEnteredWord(bottomView: View, draggedViewId: Int) {
        dragExitedTimer.cancel()
    }

    override fun dragExitedTopContainer(bottomView: View, draggedViewId: Int) {
        if (!isDraggedViewFromTopContainer) {
            flexContainerTop.removeView(shadowView)
            changeShadowViewToWordView(draggedViewId)
        }

        dragExitedTimer.start()
    }

    override fun dragExitedWord(bottomView: View, draggedViewId: Int) {
        dragExitedTimer.start()
    }

    override fun dragLocation(bottomView: View, draggedViewId: Int, x: Float) {
        addShadowView(
            draggedViewId,
            bottomView.tag.toString().toInt(),
            x < bottomView.width / 2
        )
    }

    override fun dropInTopContainer(
        bottomView: View,
        draggedViewId: Int,
        isDragViewFromTopContainer: Boolean
    ) {
        viewAddedToTopContainerByClick = false
        isJustDroppedWordInTopContainer = true

        if (isShadowViewInContainer()) {
            changeShadowViewToWordView(draggedId)
        } else {
            addViewToTopContainer(draggedId)
        }
    }

    override fun dropInBottomContainer(
        bottomView: View,
        draggedViewId: Int,
        isDragViewFromTopContainer: Boolean
    ) {
        isJustDroppedWordInBottomContainer = true

        setBottomWordViewVisible(getBottomWord(draggedId), true)
        flexContainerTop.removeView(shadowView)
        changeShadowViewToWordView(draggedViewId)
    }

    override fun dragEnded() {
        if (dragEnded)
            return
        if (isDraggedViewFromTopContainer) {
            if (!isJustDroppedWordInBottomContainer && isShadowViewInContainer()) {
                changeShadowViewToWordView(draggedId)
            }
        } else {
            if (!isJustDroppedWordInTopContainer) {
                setBottomWordViewVisible(getBottomWord(draggedId), true)
            }
        }

        dragEnded = true
        draggedId = DRAGGED_ID_DEFAULT
    }


    private fun setBottomWordViewVisible(wordView: WordView?, visible: Boolean) {
        wordView?.isVisible = visible

        val bottomShadow = wordView?.view?.findViewById<View>(R.id.bottom_shadow)
        val itemText = wordView?.view?.findViewById<TextView>(R.id.itemText)

        if (visible) {
            itemText?.visibility = View.VISIBLE
            bottomShadow?.visibility = View.GONE
        } else {
            itemText?.visibility = View.INVISIBLE
            bottomShadow?.visibility = View.VISIBLE
        }
    }


    private fun setTopWordViewLookLikeShadow(view: View?, isShadow: Boolean) {
        val topShadow = view?.findViewById<View>(R.id.top_shadow)
        val itemText = view?.findViewById<TextView>(R.id.itemText)

        if (isShadow) {
            topShadow?.visibility = View.VISIBLE
        } else {
            topShadow?.visibility = View.GONE
        }
    }


    private fun isShadowViewInContainer(): Boolean {
        for (v in flexContainerTop.iterator()) {
            if (v.tag == SHADOW_TAG) {
                return true
            }
        }
        return false
    }

    private fun changeShadowViewToWordView(draggedViewId: Int) {
        val word = getTopWord(draggedViewId)
        val topView = word?.view

        topView?.let {
            it.tag = draggedViewId
            setTopWordViewLookLikeShadow(it, false)
        }

        createShadowView()
    }

    private fun addShadowView(dragIndex: Int, belowShadowViewIndex: Int, shadowSideLeft: Boolean) {
        val topWordView = getTopWord(belowShadowViewIndex)
        val dragWordView = getTopWord(dragIndex)

        if (belowShadowViewIndex == SHADOW_TAG) {
            return
        }

        val bottomViewIndexOnList = flexContainerTop.indexOfChild(topWordView?.view)
        val shadowViewIndexOnList = flexContainerTop.indexOfChild(shadowView)

        // jezeli shadow view jest na liscie przed bottomView to od wyniku trzeba odjac 1
        val isShadowViewIsBeforeBottomView =
            shadowViewIndexOnList != -1 && shadowViewIndexOnList < bottomViewIndexOnList

        val newShadowIndex =
            bottomViewIndexOnList + (if (shadowSideLeft) 0 else 1) - (if (isShadowViewIsBeforeBottomView) 1 else 0)

        if (newShadowIndex == shadowViewIndexOnList) {
            return
        }

        dragWordView?.let { changeWordViewToShadowView(it.view, it.word) }
        flexContainerTop.removeView(shadowView)
        flexContainerTop.addView(shadowView, newShadowIndex)
    }

    private fun getTopWord(id: Int): WordView? {
        for (w in wordsTop) {
            if (w.id == id) {
                return w
            }
        }
        return null
    }

    private fun getBottomWord(id: Int): WordView? {
        for (w in wordsBottom) {
            if (w.id == id) {
                return w
            }
        }
        return null
    }

    companion object {
        private const val TAG = "FlexCard"
        private const val REMOVE_SHUTTLE_AFTER_ANIMATION_DELAY = 300L
        private const val SHADOW_TAG = -100
        private const val BOTTOM_VIEW_CLICK_INDEX_DEFAULT = -110
        private const val DRAGGED_ID_DEFAULT = -120

    }
}