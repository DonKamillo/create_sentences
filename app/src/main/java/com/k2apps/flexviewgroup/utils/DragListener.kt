package com.k2apps.flexviewgroup.utils

import android.util.Log
import android.view.DragEvent
import android.view.DragEvent.ACTION_DRAG_LOCATION
import android.view.View
import android.view.View.OnDragListener

class DragListener(
    private val listener: com.k2apps.flexviewgroup.OnDragListener,
    private val dragObject: DragObject,
    private val word: String
) : OnDragListener {

    override fun onDrag(v: View, event: DragEvent): Boolean {

        when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> {
                val dragElementInfo = getDragElementInfo(event)

                if (dragObject == DragObject.TOP_CONTAINER && dragElementInfo.first) {
                    listener.dragStartedInTopContainer(v, dragElementInfo.second)
                } else if (dragObject == DragObject.BOTTOM_CONTAINER && !dragElementInfo.first) {
                    listener.dragStartedInBottomContainer(v, dragElementInfo.second)
                }
            }

            DragEvent.ACTION_DRAG_ENTERED -> {
                val draggedViewId = getDragElementInfo(event).second
                Log.e(
                    TAG,
                    "ACTION_DRAG_ENTERED ${dragObject.name}, id: $draggedViewId, isTop: ${getDragElementInfo(
                        event
                    ).first}"
                )
                when (dragObject) {
                    DragObject.TOP_CONTAINER ->
                        listener.dragEnteredTopContainer(v, draggedViewId)
                    DragObject.TOP_WORD ->
                        listener.dragEnteredWord(v, draggedViewId)
                }
            }

            DragEvent.ACTION_DRAG_EXITED -> {
                val draggedViewId = getDragElementInfo(event).second
                Log.e(
                    TAG,
                    "ACTION_DRAG_EXITED ${dragObject.name}, id: $draggedViewId, isTop: ${getDragElementInfo(
                        event
                    ).first}"
                )

                when (dragObject) {
                    DragObject.TOP_CONTAINER ->
                        listener.dragExitedTopContainer(v, draggedViewId)
                    DragObject.TOP_WORD ->
                        listener.dragExitedWord(v, draggedViewId)
                }
            }

            // ACTION_DROP wywoływane jest tylko jeżeli drop jest nad View z DragListener,
            // czyli topContainer, albo bottomContainer albo TopView,
            // nie jest wywoływane gdy drop jest poza kontenerem
            DragEvent.ACTION_DROP -> {
                val dragElementInfo = getDragElementInfo(event)
                Log.e(
                    TAG,
                    "ACTION_DROP ${dragObject.name}, id: ${dragElementInfo.second}, isTop: ${dragElementInfo.first}"
                )



                when (dragObject) {
                    DragObject.TOP_CONTAINER, DragObject.TOP_WORD ->
                        listener.dropInTopContainer(
                            v,
                            dragElementInfo.second,
                            dragElementInfo.first
                        )
                    DragObject.BOTTOM_CONTAINER, DragObject.BOTTOM_WORD ->
                        listener.dropInBottomContainer(
                            v,
                            dragElementInfo.second,
                            dragElementInfo.first
                        )
                }

            }
            ACTION_DRAG_LOCATION -> {
//                Log.e(TAG, "ACTION_DRAG_LOCATION ${event.x}")
                val draggedViewId = getDragElementInfo(event).second
                when (dragObject) {
                    DragObject.TOP_WORD ->
                        listener.dragLocation(v, draggedViewId, event.x)
                }
            }

            // ACTION_DRAG_ENDED wywoływane jest dla każdego View w oknie z dodanym DragListener,
            // czyli dla topContainer, bottomContainer, i wszystkich topView + shadowView

            // nie moge robić zmian w kontenerach (ConcurrentModificationException)
            DragEvent.ACTION_DRAG_ENDED -> {
                Log.e(TAG, "ACTION_DRAG_ENDED ${dragObject.name} - $word")
                // event.clipDescription == null
                listener.dragEnded()

            }
            else -> {
//                Log.e(TAG, "else")
            }


        }

        return true
    }

    private fun getDragElementInfo(event: DragEvent): Pair<Boolean, Int> {
        val s = event.clipDescription.label.toString()

        val isDragViewFromTopContainer = s.substring(0, 1) == "T"
        val draggedViewId = s.substring(1).toInt()

        return Pair(isDragViewFromTopContainer, draggedViewId)
    }

    companion object {
        val TAG = "DragListener"
    }
}