package com.k2apps.flexviewgroup.utils

import android.content.Context
import android.util.TypedValue
import android.view.ViewGroup
import com.google.android.flexbox.FlexboxLayoutManager
import com.k2apps.flexviewgroup.R

fun setFlexItemAttributes(): FlexboxLayoutManager.LayoutParams {
    val flexItem = FlexboxLayoutManager.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT
    )

    flexItem.flexGrow = 0.0F
    flexItem.flexShrink = 1.0F
    val flexBasisPercent = -1
    flexItem.flexBasisPercent =
        if (flexBasisPercent == -1) -1f else (flexBasisPercent / 100.0).toFloat()


    return flexItem
}

fun convertDpToPixel(dp: Float, context: Context): Float {
    val metrics = context.resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics)
}

class Utils {

    companion object {
        fun getWordImageRes(context: Context, word: String): Int {
            return R.drawable.w_child
        }

        fun getWordProgressIconRes(progress: Int): Int {
            return R.drawable.ic_word_progress_5
        }
    }
}