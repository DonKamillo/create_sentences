package com.k2apps.flexviewgroup.utils

import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class TouchListener(
    private val view: View,
    private val index: Int,
    private val isTop: Boolean,
    private val listener: com.k2apps.flexviewgroup.OnTouchListener
) : View.OnTouchListener {

    private var startX = 0F
    private var startY = 0F
    private var isDrag = false

    init {
        view.setOnTouchListener(this)
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                isDrag = false
                startX = event.x
                startY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                if (abs(event.x - startX) > DRAG_MAX_X_MOVE || abs(event.y - startY) > DRAG_MAX_Y_MOVE) {
                    if (!isDrag) {
                        listener.startDrag(view, index, isTop)
                    }
                    isDrag = true
                }
            }
            MotionEvent.ACTION_UP -> {
                if (!isDrag) {
                    v?.performClick()
                }
            }
            MotionEvent.ACTION_CANCEL -> {
            }
        }
        return true
    }


    companion object {
        private const val DRAG_MAX_X_MOVE = 50f
        private const val DRAG_MAX_Y_MOVE = 50f
    }
}