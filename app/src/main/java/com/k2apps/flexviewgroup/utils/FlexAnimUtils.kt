package com.k2apps.flexviewgroup.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Rect
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.RelativeLayout

const val ANIMATION_DURATION = 1800
fun getViewItemToScalingAnimator(
    parentView: RelativeLayout?,
    viewToAnimate: View?,
    itemFromRect: Rect,
    itemToRect: Rect,
    duration: Long,
    startDelay: Long
): AnimatorSet { // get all coordinates at once
    val parentViewRect = Rect()
    val viewToAnimateRect = Rect()
    parentView!!.getGlobalVisibleRect(parentViewRect)
    viewToAnimate!!.getGlobalVisibleRect(viewToAnimateRect)
    viewToAnimate.scaleX = 1f
    viewToAnimate.scaleY = 1f

    // rescaling of the object on X-axis
    val valueAnimatorWidth = ValueAnimator.ofInt(itemFromRect.width(), itemFromRect.width())
    valueAnimatorWidth.addUpdateListener {
        // Get animated width value update
        val newWidth = valueAnimatorWidth.animatedValue as Int
        // Get and update LayoutParams of the animated view
        val lp = viewToAnimate.layoutParams as RelativeLayout.LayoutParams
        lp.width = newWidth
        viewToAnimate.layoutParams = lp
    }
    // rescaling of the object on Y-axis
    val valueAnimatorHeight = ValueAnimator.ofInt(itemFromRect.height(), itemFromRect.height())
    valueAnimatorHeight.addUpdateListener {
        // Get animated width value update
        val newHeight = valueAnimatorHeight.animatedValue as Int
        // Get and update LayoutParams of the animated view
        val lp = viewToAnimate.layoutParams as RelativeLayout.LayoutParams
        lp.height = newHeight
        viewToAnimate.layoutParams = lp
    }
    // moving of the object on X-axis
    val translateAnimatorX = ObjectAnimator.ofFloat(
        viewToAnimate,
        "X",
        itemFromRect.left - parentViewRect.left.toFloat(),
        itemToRect.left - parentViewRect.left.toFloat()
    )
    // moving of the object on Y-axis
    val translateAnimatorY = ObjectAnimator.ofFloat(
        viewToAnimate,
        "Y",
        itemFromRect.top - parentViewRect.top.toFloat(),
        itemToRect.top - parentViewRect.top.toFloat()
    )
    val animatorSet = AnimatorSet()
    animatorSet.interpolator = DecelerateInterpolator(1f)
    animatorSet.duration = duration // can be decoupled for each animator separately
    animatorSet.startDelay = startDelay // can be decoupled for each animator separately
    animatorSet.playTogether(
        valueAnimatorWidth,
        valueAnimatorHeight,
        translateAnimatorX,
        translateAnimatorY
    )
    return animatorSet
}